package com.virtusaTraining.singleTon;

public class StaticBlockSingleTon {
    private StaticBlockSingleTon(){}
    private static  StaticBlockSingleTon instace;

    static {
        try{
            instace = new StaticBlockSingleTon();
        }
        catch(Exception e){
            //Static block comes under Eager Initalization but this provides
            // exection handling
        }
    }

    public static StaticBlockSingleTon getInstace(){
        return instace;
    }
}
