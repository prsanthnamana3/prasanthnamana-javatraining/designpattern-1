package com.virtusaTraining.singleTon;

public class SingleTonLazyLoad {
    private static SingleTonLazyLoad instance ;
    private SingleTonLazyLoad(){}

    public static SingleTonLazyLoad getInstance(){
        if(instance == null)
            instance = new SingleTonLazyLoad();
        return instance;
    }
}
