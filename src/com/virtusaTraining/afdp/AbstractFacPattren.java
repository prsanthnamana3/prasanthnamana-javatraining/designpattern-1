package com.virtusaTraining.afdp;

public class AbstractFacPattren {
    private Garden garden;
    public Garden gertGarden(String type){
        if(type.equals("veggie")) garden =  new VeggieGarden();
        else garden =  new SomeGarden();
        return garden;
    }

    public static void main(String[] args){
        AbstractFacPattren abp  = new AbstractFacPattren();
        Garden gd = abp.gertGarden("veggie");
        System.out.println(gd.gerBorder().getName());
        gd = abp.gertGarden("Not veggie");
        System.out.println(gd.gerBorder().getName());
    }
}
