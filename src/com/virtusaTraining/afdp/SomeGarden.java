package com.virtusaTraining.afdp;

public class SomeGarden extends Garden {
    @Override
    public Plant getShade() {
        return new Plant("Some Garden 1");
    }

    @Override
    public Plant gerBorder() {
        return new Plant("Some garden 2");
    }

    @Override
    public Plant gerCenter() {
        return new Plant("some Garden 3");
    }
}
