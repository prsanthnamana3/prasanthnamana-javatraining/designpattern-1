package com.virtusaTraining.afdp;

public class VeggieGarden extends Garden{
    @Override
    public Plant getShade() {
        return new Plant("Veggie 1");
    }

    @Override
    public Plant gerBorder() {
        return new Plant("Veggie 2");
    }

    @Override
    public Plant gerCenter() {
        return new Plant("Veggie 3");
    }
}
