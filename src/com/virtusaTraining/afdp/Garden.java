package com.virtusaTraining.afdp;

public abstract class Garden {

    public abstract Plant getShade();
    public abstract Plant gerBorder();
    public abstract Plant gerCenter();
}
