package com.virtusaTraining.afdp;

public class Plant {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Plant(String name) {
        this.name = name;
    }

    private String name;
}
