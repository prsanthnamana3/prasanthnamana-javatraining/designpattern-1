package com.virtusaTraining.Builder;

public class BuilderP {

    private String fname;
    private String lName;
    private  int id;

    //optional
    private String gender;
    private int age;

    public BuilderP(Builder builder) {
        this.fname = builder.fname;
        this.lName = builder.lName;
        this.id = builder.id;
        this.gender = builder.gender;
        this.age = builder.age;
    }


    public static class Builder{
        private String fname;
        private String lName;
        private  int id;
        private String gender;
        private int age;

        public Builder(String fname, String lName, int id) {
            this.fname = fname;
            this.lName = lName;
            this.id = id;
        }

        public Builder setGender(String gender){
            this.gender = gender;
            return this;
        }

        public Builder setAge(int age){
            this.age = age;
            return this;
        }

        public BuilderP build(){
            return new BuilderP(this);
        }
    }
}
