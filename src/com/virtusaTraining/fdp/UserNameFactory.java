package com.virtusaTraining.fdp;

public class UserNameFactory {
    public UserName getUserNameInstance(String userName){
        if(userName.indexOf(",")>0) return new ComaSeperate(userName);
        return new SpaceSeperated(userName);
    }

    public static void main(String[] args){
        String userName = "Prasanth Namana";
        UserNameFactory usf  = new UserNameFactory();
        System.out.println(usf.getUserNameInstance(userName).getClass().getName());
        userName = "Prasanth,Namana";
        System.out.println(usf.getUserNameInstance(userName).getClass().getName());
    }
}
